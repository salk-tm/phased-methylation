#!/usr/bin/env bash

'''run me as "bash -i phased_methylation.sh" followed by directory or file locations
this scrip is a little messy and cleaning up output file paths would really be nice

IMPORTANT, this can only be run when there are no sequencing runs in progress. If
there are they must be paused and then the guppy_basecaller needs to be killed to
clear the GPU memory. In a terminal, type nvidia-smi and find the job ID of the
guppy_basecaller(there may be two running).  KILL THEM all Run nvidia-smi again and
you should see that the GPU memory usage is very low now. Start this script.  Once
it gets to the megalodon part you can start sequencing on the PromethION again. If
you start the sequener too soon this script will crash once it gets to the megalodon
step
'''

eval "$(conda shell.bash hook)"

declare -r ref=$1
declare -r fast5=$2
declare -r outdir=$3
declare -r fastq=$4

echo "running on ref $1 or "$(echo "$ref" | cut -d. -f1)""
echo "running on fast5s $2 or $fast5"
echo "placing files here $3 or $outdir"
echo "running on fastq $4 or $fastq"

#generate reference index
minimap2 -ax map-ont -t 12 -d $1.mmi $1
samtools faidx $1

#map the Nanopore reads to the reference
minimap2 -t 12 -ax map-ont $1 $4 | samtools sort -o minimap2.ont.bam && samtools index minimap2.ont.bam

#call variants from the BAM file
longshot -O longshot.tagged.bam --bam minimap2.ont.bam --ref $1 --out longshot.vcf -F

#pull out the phased reads and genereate a read list
samtools view longshot.tagged.bam | grep "HP:i:1" | cut -f1 > read_ids_hap1.txt
samtools view longshot.tagged.bam | grep "HP:i:2" | cut -f1 > read_ids_hap2.txt
samtools view longshot.tagged.bam | grep -v "HP:i:1" | grep -v "HP:i:2" | cut -f1 > read_ids_unphased.txt

#Fix the VCF for input into megalodon
grep \## longshot.vcf > longshot.fixed.vcf
awk '{printf("##contig=<ID=%s,length=%d>\n",$1,$2);}' $1.fai >> longshot.fixed.vcf 
grep -v \## longshot.vcf >> longshot.fixed.vcf 

conda activate mega

#run megalodon with all the inputs
#may need to change the guppy-config file depending on what is being run
megalodon $2 \
    --outputs mods basecalls mod_basecalls variants variant_mappings \
    --variant-filename longshot.fixed.vcf \
    --reference $1.mmi \
    --sort-mappings \
    --devices 1 \
    --processes 16 \
    --guppy-params "-d /data/rerio/basecall_models" \
    --guppy-config res_dna_r941_prom_modbases_5mC_v001.cfg \
    --output-directory $3 \
    --guppy-server-path /usr/bin/guppy_basecall_server \
    --overwrite

#uses the reads to extract the phased variant calls
#this is super slow and can be improved by loading the basecalls.db from megalodon into memory first
megalodon_extras aggregate run --outputs mods\
    --megalodon-directory $3 \
    --read-ids-filename read_ids_hap1.txt \
    --output-suffix hp1

 megalodon_extras aggregate run --outputs mods\
    --megalodon-directory $3 \
    --read-ids-filename read_ids_hap2.txt \
    --output-suffix hp2

#this splits the 5mC bed file into CpG, Chh, and ChG files
conda activate split_bed
echo "split_methyl_bed.py $3/modified_bases.5mC.bed $1 all"
split_methyl_bed.py $3/modified_bases.5mC.bed $1 all

echo "split_methyl_bed.py $3/modified_bases.hp1.5mC.bed $1 hp1"
split_methyl_bed.py $3/modified_bases.hp1.5mC.bed $1 hp1
echo  "split_methyl_bed.py $3/modified_bases.hp2.5mC.bed $1 hp2"
split_methyl_bed.py $3/modified_bases.hp2.5mC.bed $1 hp2
